@extends('layouts.app')

@section('content')
<div class="container">
<h3>Base Calculations </h3>
            <div class="panel panel-default">
               <div class="panel-heading">
                        <div class="input-group col-lg-1">
                          <span class="input-group-addon" id="basic-addon1">Entry Date</span>
                          <input type="date" id='entryDate2' name="entryDate2" class="form-control" placeholder="Weight update entry date" >
                          <button class="btn btn-warning" class="pull-right" name="loadbs" >Load values</button>
                        </div>
                        <div class="pull-right">

                        </div>
              </div>
              <div class="panel-body">
                <div id="exampleConsole2" class="console">Click "Load" to load data from server</div>
                <div id="example2"></div>
              </div>
            </div>
</div>
<script src="/../js/BaseCalculations.js"></script>

<h3>Weight Calculations </h3>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="input-group col-lg-1">
                        <span class="input-group-addon" id="basic-addon1">Entry Date</span>
                        <input type="date" id='entryDate' name="entryDate" class="form-control" placeholder="Weight update entry date" >
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-warning" name="load" >Load values</button>
                        <button class="btn btn-success" name="save">Save values</button>
                        <label><input type="checkbox" name="autosave" checked="checked" autocomplete="off"> Autosave</label>
                    </div>
                </div>
                <div id="exampleConsole" class="console">Click "Load" to load data from server</div>
                <div id="example1"></div>
            </div>
        </div>
    </div>
</div>
<script src="/../js/Weightcalculations.js"></script>

<h3>Daily Transactions </h3>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="input-group col-lg-1">
                        <span class="input-group-addon" id="basic-addon1">Entry Date</span>
                        <input type="date" id='entryDate3' name="entryDate3" class="form-control" placeholder="Weight update entry date" >
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-warning" name="load_dt" >Load values</button>
                        <button class="btn btn-success" name="save_dt">Save values</button>
                        <label><input type="checkbox" name="autosave3" checked="checked" autocomplete="off"> Autosave</label>
                    </div>
                </div>
                <div id="exampleConsole3" class="console">Click "Load" to load data from server</div>
                <div id="example3"></div>
            </div>
        </div>
    </div>
</div>
<script src="/../js/DailyTransaction.js"></script>
@endsection
