var
  $container = $("#example1"),
  $console = $("#exampleConsole"),
  $parent = $container.parent(),
  autosaveNotification,
  hot;

hot = new Handsontable($container[0], {
  columnSorting: true,
  startRows: 8,
  startCols: 3,
  rowHeaders: true,
  colHeaders: ['Branch', 'HO_NW', 'DIFF'],
  columns: [
    {},
    {},
    {}
  ],
  minSpareCols: 0,
  minSpareRows: 1,
  contextMenu: true,
  afterChange: function (change, source) {
    var data;

    if (source === 'loadData' || !$parent.find('input[name=autosave]').is(':checked')) {
      return;
    }
    data = change[0];

    // transform sorted row to original row
    data[0] = hot.sortIndex[data[0]] ? hot.sortIndex[data[0]][0] : data[0];

    clearTimeout(autosaveNotification);
    $.ajax({
      url: '/save',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      dataType: 'json',
      type: 'POST',
      data: {data: hot.getData()}, // contains changed cells' data
      success: function () {
        $console.text('Autosaved (' + change.length + ' cell' + (change.length > 1 ? 's' : '') + ')');

        autosaveNotification = setTimeout(function () {
          $console.text('Changes will be autosaved');
        }, 1000);
      }
    });
  }
});

$parent.find('button[name=load]').click(function () {
// alert("Hi");
 var request = $.get('/load');
  $.ajax({
    url: '/load',
    dataType: 'json',
    type: 'GET',
    success: function (res) {
      var data = [], row;
      //alert(data);
      for (var i = 0, ilen = res.cars.length; i < ilen; i++) {
        row = [];
        row[0] = res.cars[i].Branch;
        row[1] = res.cars[i].Ho_nw;
        row[2] = res.cars[i].diff;
        data[res.cars[i].S_No - 1] = row;
      }
      $console.text('Data loaded');
      hot.loadData(data);
    }
  });
}).click(); // execute immediately

$parent.find('button[name=save]').click(function () {
  alert(hot.getData());

 $.ajax({
    url: '/save',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {data: hot.getData()}, // returns all cells' data
    dataType: 'json',
    type: 'POST',
    success: function (res) {
      if (res.result === 'ok') {
        $console.text('Data saved');
      }
      else {
        $console.text('Save error');
      }
    },
    error: function () {
      $console.text('Save error');
    }
  });
});

$parent.find('button[name=reset]').click(function () {
  $.ajax({
    url: 'php/reset.php',
    success: function () {
      $parent.find('button[name=load]').click();
    },
    error: function () {
      $console.text('Data reset failed');
    }
  });
});

$parent.find('input[name=autosave]').click(function () {
  if ($(this).is(':checked')) {
    $console.text('Changes will be autosaved');
  }
  else {
    $console.text('Changes will not be autosaved');
  }
});
