var
  $container = $("#example3"),
  $console = $("#exampleConsole3"),
  $parent = $container.parent(),
  autosaveNotification,
  hot3;

hot3 = new Handsontable($container[0], {
  columnSorting: true,
  startRows: 8,
  startCols: 3,
  rowHeaders: true,
  colHeaders: ['BRANCHID','BRANCH NAME','GROSS WEIGHT GMS','NET WEIGHT GMS','GROSS AMOUNT','REPORT VALUE','B C MARGIN'],
  columns: [
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true}
  ],
  minSpareCols: 0,
  minSpareRows: 1,
  contextMenu: true,
  afterChange: function (change, source) {
    var data;

    if (source === 'loadData' || !$parent.find('input[name=autosave3]').is(':checked')) {
      return;
    }
    data = change[0];

    // transform sorted row to original row
    data[0] = hot3.sortIndex[data[0]] ? hot3.sortIndex[data[0]][0] : data[0];

    clearTimeout(autosaveNotification);
    $.ajax({
      url: '/save_dt',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      dataType: 'json',
      type: 'POST',
      data: {data: hot3.getData()}, // contains changed cells' data
      success: function () {
        $console.text('Autosaved (' + change.length + ' cell' + (change.length > 1 ? 's' : '') + ')');

        autosaveNotification = setTimeout(function () {
          $console.text('Changes will be autosaved');
        }, 1000);
      }
    });
  }
});

$parent.find('button[name=load_dt]').click(function () {
   var data=$("#entryDate3").val();
   alert(data);
   //var request = $.get('/load_dt');
  $.ajax({
    url: '/load_dt',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    dataType: 'json',
    type: 'POST',
    data:{id:data},
    success: function (res) {
      var data = [], row;
      //alert(data);
      var r=0;
      for (var i = 0, ilen = res.val.length; i < ilen; i++) {
        row = [];
        row[0] = res.val[i].branch_id;
        row[1] = res.val[i].branch_name;
        row[2] = res.val[i].gross_weight_gms;
        row[3] = res.val[i].net_weight_gms;
        row[4] = res.val[i].gross_amount;
        row[5] = res.val[i].report_value;
        row[6] = res.val[i].base_calculation_margin;
        data[r] = row;
        r=r+1;
      }
      $console.text('Data loaded');
      hot3.loadData(data);
    }
  });
}).click(); // execute immediately

$parent.find('button[name=save_dt]').click(function () {
  alert(hot3.getData());

 $.ajax({
    url: '/save_dt',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {data: hot3.getData()}, // returns all cells' data
    dataType: 'json',
    type: 'POST',
    success: function (res) {
      if (res.result === 'ok') {
        $console.text('Data saved');
      }
      else {
        $console.text('Save error');
      }
    },
    error: function () {
      $console.text('Save error');
    }
  });
});


$parent.find('input[name=autosave3]').click(function () {
  if ($(this).is(':checked')) {
    $console.text('Changes will be autosaved');
  }
  else {
    $console.text('Changes will not be autosaved');
  }
});
