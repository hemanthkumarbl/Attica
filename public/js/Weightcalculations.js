var
  $container = $("#example1"),
  $console = $("#exampleConsole"),
  $parent = $container.parent(),
  autosaveNotification,
  hot;

hot = new Handsontable($container[0], {
  columnSorting: true,
  startRows: 8,
  startCols: 3,
  rowHeaders: true,
  colHeaders: ['BRANCHID','BRANCH NAME','BRANCH OFFICE GROSS WEIGHT','BRANCH OFFICE GROSS WEIGHT','HEAD OFFICE NET WEIGHT','BO HO WEIGHT DIFFERENCE','BO GROSS WEIGHT BO NET WEIGHT DIFFERENCE','MARGIN'],
  columns: [
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {},
    {
      renderer: function(instance, td, row, col, prop, value){
              var a,b;
              a = instance.getDataAtCell(row, 4);
              b = instance.getDataAtCell(row, 3);
              arguments[5] =  a - b;
              Handsontable.NumericCell.renderer.apply(this, arguments);
      }
    },
    {
      renderer: function(instance, td, row, col, prop, value){
              var c,d;
              c = instance.getDataAtCell(row, 3);
              d = instance.getDataAtCell(row, 2);
              arguments[5] =  c - d;
              Handsontable.NumericCell.renderer.apply(this, arguments);
      }
    },
    {
      renderer: function(instance, td, row, col, prop, value){
              var a,b;
              a = instance.getDataAtCell(row, 4);
              b = instance.getDataAtCell(row, 3);
              arguments[5] = (a - b) * 2600;
              Handsontable.NumericCell.renderer.apply(this, arguments);
      }
    }
  ],
  minSpareCols: 0,
  minSpareRows: 1,
  contextMenu: true,
  afterChange: function (change, source) {
    var data;

    if (source === 'loadData' || !$parent.find('input[name=autosave]').is(':checked')) {
      return;
    }
    data = change[0];

    // transform sorted row to original row
    data[0] = hot.sortIndex[data[0]] ? hot.sortIndex[data[0]][0] : data[0];

    clearTimeout(autosaveNotification);
    $.ajax({
      url: '/save',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      dataType: 'json',
      type: 'POST',
      data: {data: hot.getData()}, // contains changed cells' data
      success: function () {
        $console.text('Autosaved (' + change.length + ' cell' + (change.length > 1 ? 's' : '') + ')');

        autosaveNotification = setTimeout(function () {
          $console.text('Changes will be autosaved');
        }, 1000);
      }
    });
  }
});

$parent.find('button[name=load]').click(function () {
   var data=$("#entryDate").val();
//   alert(data);
 var request = $.get('/load');
  $.ajax({
    url: '/load',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    dataType: 'json',
    type: 'POST',
    data:{id:data},
    success: function (res) {
      var data = [], row;
      //alert(data);
      var r=0;
      for (var i = 0, ilen = res.val.length; i < ilen; i++) {
        row = [];
        row[0] = res.val[i].branch_id;
        row[1] = res.val[i].branch_name;
        row[2] = res.val[i].branch_office_gross_weight;
        row[3] = res.val[i].branch_office_net_weight;
        row[4] = res.val[i].head_office_net_weight;
        row[5] = res.val[i].bo_ho_weight_difference;
        row[6] = res.val[i].bo_gross_weight_bo_net_weight_difference;
        row[7] = res.val[i].margin;
        data[r] = row;
        r=r+1;
      }
      $console.text('Data loaded');
      hot.loadData(data);
    }
  });
}).click(); // execute immediately

$parent.find('button[name=save]').click(function () {
  alert(hot.getData());

 $.ajax({
    url: '/save',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {data: hot.getData()}, // returns all cells' data
    dataType: 'json',
    type: 'POST',
    success: function (res) {
      if (res.result === 'ok') {
        $console.text('Data saved');
      }
      else {
        $console.text('Save error');
      }
    },
    error: function () {
      $console.text('Save error');
    }
  });
});

$parent.find('button[name=reset]').click(function () {
  $.ajax({
    url: 'php/reset.php',
    success: function () {
      $parent.find('button[name=load]').click();
    },
    error: function () {
      $console.text('Data reset failed');
    }
  });
});

$parent.find('input[name=autosave]').click(function () {
  if ($(this).is(':checked')) {
    $console.text('Changes will be autosaved');
  }
  else {
    $console.text('Changes will not be autosaved');
  }
});
