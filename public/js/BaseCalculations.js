var
  $container = $("#example2"),
  $console = $("#exampleConsole2"),
  $parent = $container.parent(),
  hot2;

hot2 = new Handsontable($container[0], {
  columnSorting: true,
  startRows: 8,
  startCols: 3,
  rowHeaders: true,
  colHeaders: ['BRANCHID','BRANCH NAME','GROSS WEIGHT','NET WEIGHT','GROSS AMOUNT','NET AMOUNT','TOTAL TRANSACTIONS','EXPENCE'],
  columns: [
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},
    {readOnly:true},

  ],
  minSpareCols: 0,
  minSpareRows: 1,
  contextMenu: true,
});

$parent.find('button[name=loadbs]').click(function () {
   var data=$("#entryDate2").val();
  // alert(data);
 //var request = $.get('/loadbs');
  $.ajax({
    url: '/loadbs',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    dataType: 'json',
    type: 'POST',
    data:{id:data},
    success: function (res) {
      var data = [], row;
      //alert(data);
      var r=0;
      for (var i = 0, ilen = res.val.length; i < ilen; i++) {
        row = [];
        row[0] = res.val[i].branch_id;
        row[1] = res.val[i].branch_name;
        row[2] = res.val[i].gross_weight;
        row[3] = res.val[i].net_weight;
        row[4] = res.val[i].gross_amount;
        row[5] = res.val[i].net_amount;
        row[6] = res.val[i].total_transactions;
        row[7] = res.val[i].expense_amount;
        data[r] = row;
        r=r+1;
      }
      $console.text('Data loaded');
      hot2.loadData(data);
    }
  });
}).click(); // execute immediately
