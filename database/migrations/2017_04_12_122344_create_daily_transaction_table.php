<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch_id');
            $table->string('branch_name');
            $table->integer('gross_weight_gms');
            $table->integer('net_weight_gms');
            $table->integer('gross_amount');
            $table->integer('report_value');
            $table->integer('base_calculation_margin');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_transaction');
    }
}
