<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasecalculationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_calculation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch_id');
            $table->string('branch_name');
            $table->integer('gross_weight');
            $table->integer('net_weight');
            $table->integer('gross_amount');
            $table->integer('net_amount');
            $table->integer('total_transactions');
            $table->integer('expense_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('base_calculation');
    }
}
