<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeightCalculationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight_calculation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id');
            $table->string('branch_name');
            $table->integer('branch_office_gross_weight');
            $table->integer('branch_office_net_weight');
            $table->integer('head_office_net_weight');
            $table->integer('bo_ho_weight_difference');
            $table->integer('bo_gross_weight_bo_net_weight_difference');
            $table->integer('margin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('weight_calculation');
    }
}
