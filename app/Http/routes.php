<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::post('/load','HandsonController@load');
Route::post('/save','HandsonController@save');
Route::post('/loadbs','HandsonController@loadbs');
Route::post('/load_dt','HandsonController@load_dt');
Route::post('/save_dt','HandsonController@save_dt');
