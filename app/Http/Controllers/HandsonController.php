<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Redirect;
use DB;
use File;
use Log;


class HandsonController extends Controller
{
    public function load(Request $req)
    {
      $a=$_POST['id'];
    // $data= DB::select(DB::raw ('select * from base_calculations'));
      $data =  DB::table('weight_calculation')
                  ->select('*')
                   ->where('created_at','like',"$a%")
                    ->get();
      $result=count($data);
      Log::info($result);
      if($result > 0)
      {
      foreach ($data as $row) {
           $row_data['val'][]=$row;
         }
      return $row_data;
      }
      return 0;
    }


    public function save(Request $req)
    {
      Log::info(count($req['data']));
        $record=count($req['data']);
        $count=1;
       foreach ($req['data'] as $rows) {
         if($count < $record)
         {
           $val5= $rows[4]-$rows[3];
           $val6=$rows[3]-$rows[2];
           $val7=$val5 * 2600;
           DB::table('weight_calculation')
            ->where('branch_id', $rows[0])
              ->update(['head_office_net_weight' => $rows[4],'bo_ho_weight_difference' => $val5,'bo_gross_weight_bo_net_weight_difference' => $val6,'margin' => $val7]);
            //->update(['head_office_net_weight' => 100]);
          $count= $count + 1;
        }
      }
    }

    public function loadbs(Request $req)
    {
      $a=$_POST['id'];
      $data =  DB::table('base_calculation')
                  ->select('*')
                   ->where('created_at','like',"$a%")
                    ->get();
      $result=count($data);
      if($result > 0)
      {
      foreach ($data as $row) {
           $row_data['val'][]=$row;
         }
      return $row_data;
      }
      return 0;
    }


    public function load_dt(Request $req)
    {
      $a=$_POST['id'];
     Log::info($a);
      $data =  DB::table('daily_transaction')
                  ->select('*')
                   ->where('created_at','like',"$a%")
                    ->get();
      $result=count($data);
      if($result > 0)
      {
      foreach ($data as $row) {
           $daily_transaction['val'][] = $row;
         }
      return $daily_transaction;
      }
      return 0;
    }


    public function save_dt(Request $req)
    {
  /*    Log::info(count($req['data']));
        $record=count($req['data']);
        $count=1;
       foreach ($req['data'] as $rows) {
         if($count < $record)
         {
           $val5= $rows[4]-$rows[3];
           $val6=$rows[3]-$rows[2];
           $val7=$val5 * 2600;
           DB::table('weight_calculation')
            ->where('branch_id', $rows[0])
              ->update(['head_office_net_weight' => $rows[4],'bo_ho_weight_difference' => $val5,'bo_gross_weight_bo_net_weight_difference' => $val6,'margin' => $val7]);
            //->update(['head_office_net_weight' => 100]);
          $count= $count + 1;
        }
      } */
    }
}
